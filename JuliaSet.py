from pyglet.gl import *
from os import path


width = 500
height = 500

# otvara prozor
config = pyglet.gl.Config(double_buffer=False)
window = pyglet.window.Window(width=width, height=height, caption="Julia Set", resizable=False, config=config, visible=False)
window.set_location(100, 100)


def loadParams(shadingParamsDescr):
    global eps, m, umin, umax, vmin, vmax, cRe, cIm
    eps = list(map(float, shadingParamsDescr.readline().split()[1:]))[0]
    m = list(map(int, shadingParamsDescr.readline().split()[1:]))[0]
    umin = list(map(float, shadingParamsDescr.readline().split()[1:]))[0]
    umax = list(map(float, shadingParamsDescr.readline().split()[1:]))[0]
    vmin = list(map(float, shadingParamsDescr.readline().split()[1:]))[0]
    vmax = list(map(float, shadingParamsDescr.readline().split()[1:]))[0]
    cRe = list(map(float, shadingParamsDescr.readline().split()[1:]))[0]
    cIm = list(map(float, shadingParamsDescr.readline().split()[1:]))[0]

def getJuliaK(u0, v0):
    k = -1
    c = complex(cRe, cIm)
    z = complex(u0, v0)

    while k < m:
        r = abs(z)
        if r >= eps:
            return k

        k = k + 1
        z = z ** 2 + c

    return -1


@window.event
def on_draw():
    glBegin(GL_POINTS)
    for x0 in range(xmax):
        u0 = getComplexCoordinateU(x0)
        for y0 in range(ymax):
            v0 = getComplexCoordinateV(y0)


            k = getJuliaK(u0,v0)
            if k == -1:
                #glColor3ub(255, 255, 255)
                glColor3ub(0,0,0)
            else:
                k *= 255 // m
                glColor3ub(*rgbDict[k])

            glVertex2f(x0, y0)
    glEnd()


    glFlush()


def getComplexCoordinateU(x0):
    u0 = ((umax - umin)/xmax) * x0 + umin
    return u0

def getComplexCoordinateV(y0):
    v0 = ((vmax - vmin)/ymax) * y0 + vmin
    return v0

@window.event
def on_resize(w, h):
    global width, height
    width = w
    height = h

    global xmax, ymax
    xmax, ymax = w, h

    glViewport(0, 0, width, height)

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluOrtho2D(0, width, 0, height)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    glClearColor(1.0, 1.0, 1.0, 0.0)
    glClear(GL_COLOR_BUFFER_BIT)
    glPointSize(1.0)
    glColor3f(0.0, 0.0, 0.0)


def enterParamsFilename():
    paramsFilename = input("Ime .txt datoteke parametara fraktalnog skupa (eps, m, umin, umax, vmin, vmax, cRe, cIm) za ucitati (prazno za default: juliaParams.txt): ")
    if len(paramsFilename) == 0:
        return "juliaParams.txt"
    paramsFilepath = "./" + paramsFilename
    while not(path.exists(paramsFilepath) and paramsFilepath.endswith(".txt")):
        print("Datoteka " + paramsFilename + " ne postoji.")
        paramsFilename = input("Ime .txt datoteke parametara fraktalnog skupa (eps, m, umin, umax, vmin, vmax, cRe, cIm) za ucitati (prazno za default: juliaParams.txt): ")
        if len(paramsFilename) == 0:
            return "juliaParams.txt"
        paramsFilepath = "./" + paramsFilename

    return paramsFilepath

if __name__ == "__main__":

    paramsFilepath = enterParamsFilename()
    print("Ucitavanje datoteke ", paramsFilepath, "...")
    with open(paramsFilepath, "r") as paramsDescr:
        loadParams(paramsDescr)

    # eps = 100
    # m = 16
    # umin = -1
    # umax = 1
    # vmin = -1.2
    # vmax = 1.2
    # cRe = 0.32
    # cIm = 0.043


    rgbDict = {}
    for j in range(85):
        rgbDict[1+j] = [3*j, 0, 0]
    for j in range(85):
        rgbDict[86+j] = [0, 3*j, 0]
    for j in range(85):
        rgbDict[171+j] = [0, 0, 3*j]


    window.set_visible(True)

    pyglet.app.run()