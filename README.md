Fractals Visualizing Mandelbrot and Julia Set. Implemented in Python using pyglet, Python OpenGL interface.

My lab assignment in Interactive Computer Graphics, FER, Zagreb.

Docs in "DokumentacijaIRGLabosi" under "8. VJEZBA."

Created: 2020
